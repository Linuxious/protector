#! /usr/bin/python
import gi, subprocess, os, backup
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

def write_config(uuid=[],dest=[],source=[],auto=True):
    settings_file = open("settings.py", "w")
    settings_file.writelines(["dest="+ str(dest), "\nsource=" + str(source), "\nauto=" + str(auto)])

if not os.path.isfile("settings.py"):
    write_config()

import settings

class StackWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title ="Protector")
        self.set_border_width(10)

        vbox = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
        self.add(vbox)

        stack = Gtk.Stack()
        stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        stack.set_transition_duration(1000)

        main = MainPage
        stack.add_titled(main(), "mainpage", "Main")

        recover = RecoverPage
        stack.add_titled(recover(), "recoverpage", "Recover")

        settings = SettingsPage
        stack.add_titled(settings(), "settingspage", "Settings")

        stack_switcher = Gtk.StackSwitcher()
        stack_switcher.set_stack(stack)
        vbox.pack_start(stack_switcher, True, True, 0)
        vbox.pack_start(stack, True, True, 0)

class MainPage(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation = Gtk.Orientation.VERTICAL, spacing = 5)

        logo = Gtk.Image()
        logo.set_from_file("./icons/Protector_logo.png")
        self.add(logo)

        self.backup_bar = Gtk.ProgressBar()
        self.add(self.backup_bar)

        bbackup = Gtk.Button(label="Backup")
        bbackup.connect("clicked", self.backup)
        bcancel = Gtk.Button(label="Cancel")

        self.add(bbackup)
        self.add(bcancel)

        self.show_all()


    def backup(self, widget):
        scan = backup.Backup().scan(settings.source)


class SettingsPage(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation = Gtk.Orientation.VERTICAL, spacing = 20)
        self.set_border_width(10)

        settings_stack = Gtk.Stack()
        settings_stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT)
        settings_stack.set_transition_duration(1000)
        settings_stack.add_titled(self.dest(), "destinations", "Destinations")
        settings_stack.add_titled(self.source(), "sources", "Sources")

        abox = Gtk.Box()
        alabel = Gtk.Label(label="Automatic Backup", xalign=0)
        auto_switch = Gtk.Switch()
        auto_switch.set_active(settings.auto)
        abox.add(alabel)
        abox.add(auto_switch)
        self.add(abox)
        settings_switcher = Gtk.StackSwitcher()
        settings_switcher.set_stack(settings_stack)
        self.pack_start(settings_switcher, True, True, 0)
        self.pack_start(settings_stack, True, True, 0)
        bapply = Gtk.Button.new_with_label("Apply")
        self.add(bapply)
        self.show_all()

    def dest(self):
        box2 = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
        scrollable_selector = Gtk.ScrolledWindow()
        scrollable_selector.set_min_content_width(180)
        scrollable_selector.set_vexpand(True)
        titles_init = [["sdb", "200G", "/"], ["sda", "200G", "/test"], ["sdc", "200G", "/dev/test"]]
        titles = Gtk.ListStore(str, str, str)
        for title in titles_init:
            titles.append(title)

        drive_selector = Gtk.TreeView(model=titles)
        renderer = Gtk.CellRendererText()
        name_column = Gtk.TreeViewColumn("Name", renderer, text=0)
        size_column = Gtk.TreeViewColumn("Size", renderer, text=1)
        mount_column = Gtk.TreeViewColumn("Mounted", renderer, text=2)
        drive_selector.append_column(name_column)
        drive_selector.append_column(size_column)
        drive_selector.append_column(mount_column)
        scrollable_selector.add(drive_selector)

        for label in settings.dest:
            row = Gtk.ListBoxRow()
            box = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
            box.pack_start(Gtk.Label(label=label, xalign=0), True, True, 0)
            row.add(box)
            #drive_selector.add(row)

        changes_box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing = 5)
        badd = Gtk.Button.new_with_label("Add")
        bremove = Gtk.Button.new_with_label("Remove")
        changes_box.add(badd)
        changes_box.add(bremove)
        box2.add(scrollable_selector)
        box2.add(changes_box)

        return box2

    def source(self):
        box2 = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)

        scrollable_selector = Gtk.ScrolledWindow()
        scrollable_selector.set_min_content_width(180)
        scrollable_selector.set_vexpand(True)
        titles_init = [["/home", "200G"], ["/home/isaac/Documents", "200G"], ["/home/isaac/Pictures", "200G"]]
        titles = Gtk.ListStore(str, str)
        for title in titles_init:
            titles.append(title)

        src_selector = Gtk.TreeView(model=titles)
        renderer = Gtk.CellRendererText()
        name_column = Gtk.TreeViewColumn("Name", renderer, text=0)
        size_column = Gtk.TreeViewColumn("Size", renderer, text=1)
        src_selector.append_column(name_column)
        src_selector.append_column(size_column)
        scrollable_selector.add(src_selector)

        for label in settings.source:
            row = Gtk.ListBoxRow()
            box = Gtk.Box(orientation = Gtk.Orientation.HORIZONTAL)
            box.pack_start(Gtk.Label(label=label, xalign=0), True, True, 0)
            row.add(box)

        changes_box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL, spacing = 5)
        badd = Gtk.Button.new_with_label("Add")
        bremove = Gtk.Button.new_with_label("Remove")
        changes_box.add(badd)
        changes_box.add(bremove)
        box2.add(scrollable_selector)
        box2.add(changes_box)

        return box2

class RecoverPage(Gtk.Box):
    def __init__(self):
        Gtk.Box.__init__(self, orientation = Gtk.Orientation.VERTICAL, spacing = 5)
        self.set_border_width(20)

        selection_grid = Gtk.Grid()
        disk = Gtk.Label(label="N/A")
        add_disk = Gtk.Button(label="Select")
        backup = Gtk.Label(label="N/A")
        add_backup = Gtk.Button(label="Select")
        backup_bar = Gtk.ProgressBar()
        brecover = Gtk.Button.new_with_label("Recover")
        bcancel = Gtk.Button.new_with_label("Cancel")

        selection_grid.add(disk)
        selection_grid.attach_next_to(add_disk, disk, Gtk.PositionType.RIGHT,1,1)
        selection_grid.attach_next_to(backup, disk, Gtk.PositionType.BOTTOM,1,1)
        selection_grid.attach_next_to(add_backup, backup, Gtk.PositionType.RIGHT,1,1)

        self.add(selection_grid)
        self.add(backup_bar)
        self.add(brecover)
        self.add(bcancel)
        self.show_all()



    def recover(self, widget):
        pass

def find_disks():
    accepted_types = ["rom", "part", "mpath"]

    disks = subprocess.check_output(["lsblk", "-anrbo", "NAME"]).decode().split("\n")
    disk_types = subprocess.check_output(["lsblk", "-anrbo", "TYPE"]).decode().split("\n")
    disk_uuids = subprocess.check_output(["lsblk", "-anrbo", "UUID"]).decode().split("\n")
    available = []
    disk_num = 0
    for types in disk_types:
        if types in accepted_types:
            available.append(disks[disk_num])
        else:
            del disk_uuids[disk_num]
        disk_num += 1
    return available, disk_uuids


def find_size():
    return subprocess.check_output(["du", "-sh", "."]).split()[0].decode()

app = StackWindow()
app.connect("destroy", Gtk.main_quit)
app.show_all()
Gtk.main()
