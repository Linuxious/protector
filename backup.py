import sys,os,gnupg,time,subprocess,shutil,hashlib

class Backup:

    def backup(self, src, path):
        shutil.make_archive(path + time.strftime("%Y-%m-%d@%H:%M:%S"), "bzip2", src)

    def scan(self, src):
        sources = []
        for s in src:
            files = []
            scans = []
            size = subprocess.check_output(["du", "-sh", s]).split()[0].decode()
            for root, directories, files in os.walk(s, topdown=False):
                for name in files:
                    scans.append(os.path.join(root, name))

            sources.append([s, [scans,str(size)]])
        return sources

    def verify(self, files, backup_files):
        for f in files:
            v1 = hashlib.md5(f)
            v2 = hashlib.md5(backup_files[files.index(f)])
            if v2 == v1:
                pass



    def encrypt(self, password, src):
        self.gpg = gnupg.GPG(gnupghome=".")
        input_data = self.gpg.gen_key_input(passphrase=password)
        key = self.gpg.gen_key(input_data)
        ascii_armored_public_keys = self.gpg.export_keys(key.fingerprint)
        ascii_armored_private_keys = self.gpg.export_keys(
            keyids=key.fingerprint,
            secret=True,
            passphrase=self.password,
        )
        key_file = open("password.gpg", "w")
        key_file.write(ascii_armored_public_keys)
        key_file.write(ascii_armored_private_keys)
        key_file = open("password.gpg")
        key_data = key_file.read()
        import_result = self.gpg.import_keys(key_data)
        user_files = open(src, "rb")
        str(self.gpg.encrypt_file(
            file=user_files,
            output=src + "gpg",
        ))

class Recover:

    def recover(self, src, dest):
        shutil.unpack_archive(src, dest, "bzip2")

    def decrypt(self, password):
        key_file = open("passwords.gpg")
        key_data = key_file.read()
        import_result = self.gpg.import_keys(key_data)

        user_files = open(src, "rb")
        check = self.gpg.decrypt_file(
            file=user_files,
            passphrase=self.password,
            output=src + ".bz",
        )
        print(check.ok)
        print(os.listdir("."))

        if check:
            return 0
        else:
            return 1
