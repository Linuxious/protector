import subprocess, settings, os, time


while True:
    for uuid in settings.uuid:
        if os.path.exists("/dev/disk/by-uuid/" + uuid):
            if not os.path.exists("/run/media/" + uuid):
                os.mkdir("/run/media/" + uuid)
                os.chmod("/run/media/" + uuid, 777)
                subprocess.run(["mount", uuid, "/run/media" + uuid])

    time.sleep(5)
