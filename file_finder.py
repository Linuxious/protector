import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class FolderWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="File to Recover")

        dialog = Gtk.FileChooserDialog(
            title="Please choose a folder", parent=self,
            action=Gtk.FileChooserAction.SELECT_FOLDER
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            "Select",
            Gtk.ResponseType.OK,
        )

        dialog.run()
        dialog.destroy()


win = FolderWindow()
win.connect("destroy", Gtk.main_quit)
win.show_all()
Gtk.main()
