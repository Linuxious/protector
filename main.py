import gi, time, requests
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib


class StackWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title ="Protector")

        box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
        self.add(box)
        logo = Gtk.Image()
        logo.set_from_file("./icons/Protector_logo.png")
        button = Gtk.Button(label="Press")
        button.connect("clicked", self.test)
        self.bar = Gtk.ProgressBar()
        box.pack_start(logo, True, True, 0)
        box.pack_start(button, True, True, 0)
        box.pack_start(self.bar, True, True, 0)


    def test(self, widget):

        files = [1,2,3,4,5,6,7,7,8,9,0]

        for f in files:
            new_value = self.bar.get_fraction() + files.index(f) / len(files)
            self.bar.set_fraction(new_value)
            time.sleep(0.5)


version = requests.get("https://gitlab.com/Linuxious/protector/-/raw/master/version").content.decode()
print(version)
app = StackWindow()
app.connect("destroy", Gtk.main_quit)
app.show_all()
Gtk.main()
